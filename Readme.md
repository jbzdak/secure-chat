# Secure Chat

My attempt to implement simple signal-like 
chat. 


## Disclaimer

1. Please  don't use it it was designed without proper cryptography review. 
2. Count of professionals reviewing this == 0; 

## Road map 

1. **Done** write services API and implement  in-memory Python service implementation.

   Here also logical format for messages between client and server is defined (I mean fields in each message.) 
2. **In progress** write (framework agnostic) controller code that implements the server side; 
3. **TODO** Write Python client code that integrates
   directly with the controller code. 
   
   This checks both sides of encryption works.
   
   Here we also define format for unencrypted message
   (server sees encrypted messages that are bytes). 
4. **TODO** Create persistent services. 

   Since it looks like I will manage implementing this on top of (couple of) 
   Key-Value stores I probably won't bother with using postgres :) its a fun toy 
   project. 
   
5. **TODO** Expose services as an web HTTP api; 

6. **TODO** Create simple HTML client; 

   NOTE: I don't intend this client to be final one, it's just transition proof of concept 
   to something more secure. 
   
7. **TODO**: Fix up some better cryptography, something with forward secrecy, or 
   better     

   1. 

7. **TODO** try to create a cross-platform client that will work as a single binary, 
   and/or could be packaged. 
   
8. **TODO** Create alternative server implementations, eg: 

   * C/C++ (for speed); 
   * PHP for cheap hosting;
   * Google App Engine (for cheap simple hosting); 
   
              
   
    