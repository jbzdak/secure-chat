import abc
import os
from typing import Dict

from secure_channel.server.api import Participant

from secure_channel.crypto import new_uid, verify_ecdsa
from ..api import Channel, Challenge, ChallengeResponse, PublicKeyAlgo


ASYM_REGISTRY: Dict[PublicKeyAlgo, "AsymetricCryptographyController"] = {}


def register_asymetric_controller(algo: PublicKeyAlgo):
    def internal(type):
        ASYM_REGISTRY[algo] = type
        return type

    return internal


def get_asymetric_controller(algo: PublicKeyAlgo):
    return ASYM_REGISTRY[algo]


class AsymetricCryptographyController(abc.ABC):
    def new_challenge(self, channel: Channel, participant: Participant) -> Challenge:
        raise NotImplementedError

    def verify_challenge(
        self,
        channel: Channel,
        participant: Participant,
        challenge: Challenge,
        response: ChallengeResponse,
    ):
        raise NotImplementedError
