from secure_channel.server.api import PublicKeyAlgo

import os

from .api import AsymetricCryptographyController, register_asymetric_controller
from ..api import Channel, Participant, Challenge, ChallengeResponse
from ...crypto import new_uid, verify_ecdsa


@register_asymetric_controller(PublicKeyAlgo.ECDSA)
class ElipticCrypto(AsymetricCryptographyController):
    def new_challenge(self, channel: Channel, participant: Participant) -> Challenge:
        assert channel.meta.public_algorithm == PublicKeyAlgo.ECDSA

        return Challenge(id=new_uid(), challenge=os.urandom(32))

    def verify_challenge(
        self,
        channel: Channel,
        participant: Participant,
        challenge: Challenge,
        response: ChallengeResponse,
    ):
        assert channel.meta.public_algorithm == PublicKeyAlgo.ECDSA

        data = challenge.challenge
        signature = response.response
        return verify_ecdsa(data, signature, participant.sign_public_key)
