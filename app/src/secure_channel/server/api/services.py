"""

"""

__all__ = [
    "OnMessage",
    "ChannelService",
    "MissingEntryException",
    "MessageService",
    "SessionService",
    "SearchDirection",
    "MissingChannelException",
    "ChallengeService",
    "MissingChallengeException",
    "DuplicateChallengeException",
]

import abc
import enum
import typing
from typing import Optional

from .entities import *


class MissingEntryException(Exception):
    """
    Raised when entry can't be found by ID.
    """


class MissingChannelException(MissingEntryException):
    def __init__(self, channel_id: bytes):
        self.channel_id = channel_id
        super().__init__()


class MissingChallengeException(MissingEntryException):
    def __init__(self, challenge_id: bytes):
        self.challenge_id = challenge_id
        super().__init__()


class DuplicateChallengeException(Exception):
    def __init__(self, challenge_id: bytes):
        self.challenge_id = challenge_id
        super().__init__()


OnMessage = typing.Callable[[Message], None]


class ChannelService(abc.ABC):
    @abc.abstractmethod
    def get_by_id(self, id: UID) -> Channel:
        pass

    @abc.abstractmethod
    def create_channel(self, meta: ChannelMeta, owner: Participant) -> Channel:
        pass

    @abc.abstractmethod
    def remove_channel(self, channel_id: UID):
        pass

    @abc.abstractmethod
    def add_participant(self, channel_id: UID, participant: Participant) -> UID:
        pass

    @abc.abstractmethod
    def remove_participant(self, channel_id: UID, participant: Participant):
        pass


class SearchDirection(enum.Enum):
    AFTER = "AFTER"
    BEFORE = "BEFORE"


class MessageService(abc.ABC):
    @abc.abstractmethod
    def post(self, channel: Channel, message: Message) -> Message:
        """Posts message and returns it's UUID."""

    @abc.abstractmethod
    def get_messages(
        self,
        channel: Channel,
        anchor: MessageIterator,
        direction: SearchDirection = SearchDirection.AFTER,
        page=100,
    ) -> typing.Sequence[Message]:
        pass

    @abc.abstractmethod
    def subscribe(self, channel, callback: OnMessage) -> UID:
        pass

    @abc.abstractmethod
    def unscribe(self, channel: Channel, sub_id: UID):
        pass

    @abc.abstractmethod
    def get_channel_meta(self, channel: Channel) -> ChannelMessageMeta:
        pass


class SessionService(abc.ABC):
    @property
    @abc.abstractmethod
    def ttl_sec(self) -> float:
        pass

    @abc.abstractmethod
    def create_session(self) -> SessionObject:
        pass

    @abc.abstractmethod
    def get_by_id(self, session_cookie: str) -> SessionObject:
        """
        Returns session associated with session cookie. If can't be found
        new object is created
        """
        pass

    @abc.abstractmethod
    def update_session(self, session_object: SessionObject) -> str:
        """
        Returns new session cookie string to send to client.
        """
        pass

    @abc.abstractmethod
    def expire_old_sessions(self):
        pass


class ChallengeService(abc.ABC):
    @property
    @abc.abstractmethod
    def ttl_sec(self) -> float:
        pass

    @abc.abstractmethod
    def store_challenge(
        self, participant: Participant, challenge: Challenge, channel: Channel
    ) -> ChallengeStore:
        pass

    @abc.abstractmethod
    def get_by_id(self, key: bytes) -> ChallengeStore:
        pass

    @abc.abstractmethod
    def expire(self):
        pass
