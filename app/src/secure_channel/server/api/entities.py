__all__ = [
    "UID",
    "Message",
    "Participant",
    "PublicKeyAlgo",
    "SymmetricAlgo",
    "ChannelMeta",
    "Channel",
    "Challenge",
    "ChallengeResponse",
    "SessionObject",
    "MessageIterator",
    "ChannelMessageMeta",
    "BaseEntity",
    "ChallengeStore",
]

import dataclasses
import enum
from collections import defaultdict
from datetime import datetime
from typing import Tuple, Optional, Mapping, Set, Dict

UID = bytes
"""
Base64 encoded constant length string
"""

MessageIterator = bytes


class BaseEntity:
    @classmethod
    def create(cls, **kwargs):
        return cls(**kwargs)


@dataclasses.dataclass(frozen=True)
class MessageKey(BaseEntity):
    """Key used to encrypt the message, """

    participant_id: UID
    key: bytes


@dataclasses.dataclass(frozen=True)
class Message(BaseEntity):
    """
    Note: Lack of "sender" is by-design. Impersonating others is for plausible
    deniability.
    """

    id: UID = dataclasses.field(compare=True)
    iterator: MessageIterator = dataclasses.field(compare=False)
    """
    This is another ID of the message, used by the MessageService to iterate 
    over previous and next messages.
    
    Format for iterator is unspecified, might change between service implementations. 
    Also iterator might change by itself.  
    """
    date_added: datetime = dataclasses.field(compare=False)
    message: bytes = dataclasses.field(compare=False)
    message_keys: Tuple[MessageKey, ...] = dataclasses.field(compare=False)
    """Keys used to encrypt the message for recipients."""

    @classmethod
    def create(cls, *, message_keys, **kwargs):
        message_keys = tuple(message_keys)
        return super().create(message_keys=message_keys, **kwargs)


@dataclasses.dataclass(frozen=True)
class Participant(BaseEntity):
    """
    NOTE: Lack of "name" is by-design, participant is a reference to public key that
    can have access to channel. They can send any message --- let's have plausible
    deniablility.
    """

    id: UID = dataclasses.field(compare=True)
    sign_public_key: bytes = dataclasses.field(compare=False)
    encrypt_public_key: bytes = dataclasses.field(compare=False)
    expiry: Optional[datetime] = dataclasses.field(compare=False)


class PublicKeyAlgo(enum.Enum):

    RSA = "RSA_4096"
    ECDSA = "ECDSA"  # Specify a curve


class SymmetricAlgo(enum.Enum):

    AES_128 = "AES_128"
    AES_256 = "AES_256"


@dataclasses.dataclass(frozen=True)
class ChannelMeta(BaseEntity):
    public_algorithm: PublicKeyAlgo
    symmetric_algorithm: SymmetricAlgo


@dataclasses.dataclass(frozen=True)
class Channel(BaseEntity):
    id: UID = dataclasses.field(compare=True)
    participants: Tuple[Participant, ...] = dataclasses.field(compare=False)
    meta: ChannelMeta = dataclasses.field(compare=False)

    @classmethod
    def create(cls, *, participants, **kwargs):
        participants = tuple(participants)
        return super().create(participants=participants, **kwargs)


@dataclasses.dataclass(frozen=True)
class ChannelMessageMeta:

    channel_id: UID
    first_message: Optional[MessageIterator]
    last_message: Optional[MessageIterator]


@dataclasses.dataclass(frozen=True)
class Challenge(BaseEntity):
    id: UID = dataclasses.field(compare=True)
    challenge: bytes = dataclasses.field(compare=False)


@dataclasses.dataclass(frozen=True)
class ChallengeResponse(BaseEntity):
    id: UID = dataclasses.field(compare=True)
    response: bytes = dataclasses.field(compare=False)


@dataclasses.dataclass()
class SessionObject(BaseEntity):
    id: UID = dataclasses.field(compare=True)
    channel_with_access: Optional[UID] = None
    logged_in_particpant: Optional[UID] = None


@dataclasses.dataclass(frozen=True)
class ChallengeStore:
    participant: Participant
    channel: Channel
    challenge: Challenge
