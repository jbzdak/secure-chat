import base64
import datetime
import logging
import threading
import typing
from collections import defaultdict, deque
from dataclasses import dataclass, field, replace
from typing import List, Dict, Tuple, Optional, Set, Deque

from .api import *
from .api.services import SearchDirection

from ..crypto import new_uid

LOGGER = logging.Logger(__name__)


@dataclass(frozen=True)
class ChannelServiceImpl(ChannelService):

    _channels: Dict[UID, Channel] = field(default_factory=dict)

    _lock: threading.RLock = field(default_factory=threading.RLock)

    def get_by_id(self, id: UID) -> Channel:
        with self._lock:
            try:
                return self._channels[id]
            except KeyError as e:
                raise MissingChannelException(channel_id=id) from e

    def create_channel(self, meta: ChannelMeta, owner: Participant) -> Channel:
        assert owner.id is None
        with self._lock:
            owner = replace(owner, id=new_uid())
            channel = Channel.create(id=new_uid(), participants=(owner,), meta=meta)
            self._channels[channel.id] = channel
            return channel

    def remove_channel(self, channel_id: UID):
        with self._lock:
            del self._channels[channel_id]

    def add_participant(self, channel_id: UID, participant: Participant) -> UID:
        assert participant.id is None
        with self._lock:
            channel = self.get_by_id(channel_id)
            participant = replace(participant, id=new_uid())
            participants = set(channel.participants)
            participants.add(participant)
            channel = replace(channel, participants=tuple(participants))
            self._channels[channel.id] = channel
            return participant.id

    def remove_participant(self, channel_id: UID, participant: Participant):
        with self._lock:
            channel = self.get_by_id(channel_id)

            new_participants = tuple(
                [p for p in channel.participants if p.id != participant.id]
            )

            channel = replace(channel, participants=new_participants)
            self._channels[channel.id] = channel


@dataclass(frozen=True)
class Subscription:
    id: UID = field(compare=True)
    callback: OnMessage = field(compare=False)

    def launch_callback(self, message: Message):
        self.__dict__["callback"](message)


@dataclass(frozen=True)
class MessageServiceImpl(MessageService):

    _messages: Dict[UID, List[Message]] = field(
        default_factory=lambda: defaultdict(list)
    )
    """
    Implicit assumption is that messages are sorted by date.    
    Dates should be UTC. 
    """

    _listeners: Dict[UID, Set[Subscription]] = field(
        default_factory=lambda: defaultdict(set)
    )

    _lock: threading.RLock = field(default_factory=threading.RLock)

    def get_channel_meta(self, channel: Channel) -> ChannelMessageMeta:
        with self._lock:
            messages = self._messages[channel.id]
            if len(messages) == 0:
                return ChannelMessageMeta(channel.id, None, None)
            return ChannelMessageMeta(
                channel.id,
                str(0).encode("ascii"),
                str(len(messages) - 1).encode("ascii"),
            )

    def post(self, channel: Channel, message: Message) -> Message:
        with self._lock:
            messages = self._messages[channel.id]
            updated_date_added = datetime.datetime.utcnow()

            # Maintain an invariant that dates are sorted by time,
            # So if we have in channel message from the "future"
            # we just update date so new messages are also from the same
            # date.
            # This **should** not happen when we are properly synchronized
            # with time source.
            if len(messages) != 0:
                last_message = messages[-1]
                if message.date_added < last_message.date_added:
                    updated_date_added = last_message.date_added

            message = replace(
                message,
                date_added=updated_date_added,
                id=new_uid(),
                iterator=str(len(messages)).encode("ascii"),
            )

            messages.append(message)

            for subscription in self._listeners.get(channel.id, []):
                try:
                    subscription.launch_callback(message)
                except:
                    pass
                    # TODO: Handle broken subscriptions

            return message

    def get_messages(
        self,
        channel: Channel,
        anchor: MessageIterator,
        direction: SearchDirection = SearchDirection.AFTER,
        page=100,
    ) -> typing.Sequence[Message]:
        assert anchor is not None
        with self._lock:
            messages = self._messages.get(channel.id, [])
            from_index = int(anchor.decode("ascii"))
            if direction == SearchDirection.AFTER:
                return tuple(messages[from_index : from_index + page])
            return tuple(messages[from_index - page + 1 : from_index + 1])

    def subscribe(self, channel, callback: OnMessage) -> UID:
        with self._lock:
            subscription_id = new_uid()
            self._listeners[channel.id].add(
                Subscription(id=subscription_id, callback=callback)
            )
            return subscription_id

    def unscribe(self, channel: Channel, sub_id: UID):
        with self._lock:
            self._listeners[channel.id].discard(
                Subscription(id=sub_id, callback=lambda x: None)
            )


T = typing.TypeVar("T")


@dataclass(frozen=True)
class ObjectWithTTL(typing.Generic[T]):

    object: T
    expiry: datetime.datetime


@dataclass(frozen=True)
class TTLStore(typing.Generic[T]):

    ttl: float
    _items: Dict[bytes, ObjectWithTTL[T]] = field(default_factory=dict)
    _expiry_queue: Deque[Tuple[datetime.datetime, bytes]] = field(default_factory=deque)
    _lock: threading.RLock = field(default_factory=threading.RLock)

    def handle_duplicate_entry(self, existing: ObjectWithTTL[T], key: bytes, obj: T):
        raise ValueError("Duplicate entry in store")

    def set(self, key: bytes, obj: T):
        with self._lock:
            existing = self._items.get(key)
            if existing is not None:
                self.handle_duplicate_entry(existing, key, obj)
                return
            expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=self.ttl)
            self._expiry_queue.append((expiry, key))
            self._items[key] = ObjectWithTTL(obj, expiry)

    def get(self, key: bytes) -> T:
        with self._lock:
            obj = self._items[key]
        if obj.expiry < datetime.datetime.utcnow():
            raise KeyError(key)
        return obj.object

    def expire(self):
        with self._lock:
            if not self._expiry_queue:
                return
            now = datetime.datetime.utcnow()
            expiry, key = self._expiry_queue[0]
            while expiry < now:
                self._items.pop(key)
                self._expiry_queue.popleft()
                expiry, key = self._expiry_queue[0]


class SessionTTLStore(TTLStore[SessionObject]):
    def handle_duplicate_entry(
        self, existing: ObjectWithTTL[SessionObject], key: bytes, obj: SessionObject
    ):
        self._items[key] = ObjectWithTTL(obj, existing.expiry)


class ChallengeTTLStore(TTLStore[ChallengeStore]):
    def handle_duplicate_entry(
        self, existing: ObjectWithTTL[ChallengeStore], key: bytes, obj: ChallengeStore
    ):
        raise DuplicateChallengeException(key)


@dataclass(frozen=True)
class SessionServiceImpl(SessionService):
    """
    Stupidly simplified session service.

    It has following assumptions:

    1. All sessions have the same TTL.
    2. If session expires between get_session and update_session calls it is dropped;
    3. Using a session does not prolong it's expiry;
    """

    @classmethod
    def create(cls, ttl: float):
        return cls(_store=SessionTTLStore(ttl))

    _store: TTLStore[SessionObject]

    @property
    def ttl_sec(self) -> float:
        return self._store.ttl

    def __new_session_id(self):
        return base64.b64encode(new_uid())

    def create_session(self) -> SessionObject:
        session = SessionObject(id=self.__new_session_id())
        self._store.set(session.id, session)
        return session

    def get_by_id(self, session_cookie: str) -> SessionObject:
        key = session_cookie.encode("ascii", "xmlcharrefreplace")
        with self._store._lock:
            try:
                return self._store.get(key)
            except KeyError:
                session = self.create_session()
                self._store.set(session.id, session)
            return session

    def update_session(self, session_object: SessionObject) -> str:
        self._store.set(session_object.id, session_object)
        return session_object.id.decode("ascii")

    def expire_old_sessions(self):
        self._store.expire()


@dataclass(frozen=True)
class ChallengeServiceImpl(ChallengeService):
    @classmethod
    def create(cls, ttl: float):
        return cls(_store=ChallengeTTLStore(ttl))

    _store: TTLStore[ChallengeStore]

    @property
    def ttl_sec(self) -> float:
        return self._store.ttl

    def store_challenge(
        self, participant: Participant, challenge: Challenge, channel: Channel
    ) -> ChallengeStore:
        store = ChallengeStore(
            participant=participant, challenge=challenge, channel=channel
        )
        self._store.set(challenge.id, store)
        return store

    def get_by_id(self, key: bytes) -> ChallengeStore:
        try:
            return self._store.get(key)
        except KeyError as e:
            raise MissingChallengeException(key) from e

    def expire(self):
        self._store.expire()
