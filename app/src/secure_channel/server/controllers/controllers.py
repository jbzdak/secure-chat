import dataclasses
from typing import Optional

from secure_channel.python_api_tests import ServiceBundle

from secure_channel.server import api
from secure_channel.server import crypto


class NoChannelPermission(Exception):
    def __init__(self, channel_id: bytes):
        self.channel_id = channel_id
        super().__init__()


def raise_on_missing_channel(inner):
    def wrapper(self, *args, **kwargs):
        try:
            return inner(self, *args, **kwargs)
        except api.MissingChannelException as e:
            raise NoChannelPermission(e.channel_id)

    return wrapper


@dataclasses.dataclass()
class RequestController:

    session_cookie: str
    bundle: ServiceBundle
    session: api.SessionObject = None  # type: ignore

    def __check_owner(self, channel: api.Channel):

        if channel.id != self.session.logged_in_particpant:
            raise NoChannelPermission(channel.id)
        participant_id = self.session.logged_in_particpant
        if participant_id not in channel.participants:
            raise NoChannelPermission(channel.id)

    def create_channel(
        self, meta: api.ChannelMeta, owner: api.Participant
    ) -> api.Channel:
        channel = self.bundle.channel.create_channel(meta, owner)
        self.session.logged_in_particpant = owner.id
        self.session.channel_with_access = channel.id
        return channel

    @raise_on_missing_channel
    def remove_channel(self, channel_id: api.UID):
        channel = self.bundle.channel.get_by_id(channel_id)
        self.__check_owner(channel)
        self.bundle.channel.remove_channel(channel_id)

    @raise_on_missing_channel
    def add_participant(
        self, channel_id: api.UID, participant: api.Participant
    ) -> api.UID:
        channel = self.bundle.channel.get_by_id(channel_id)
        self.__check_owner(channel)
        return self.bundle.channel.add_participant(channel_id, participant)

    @raise_on_missing_channel
    def remove_participant(self, channel_id: api.UID, participant: api.Participant):
        channel = self.bundle.channel.get_by_id(channel_id)
        self.__check_owner(channel)
        self.bundle.channel.remove_participant(channel_id, participant)

    @raise_on_missing_channel
    def post_message(self, channel_id: api.UID, message: api.Message) -> api.Message:
        channel = self.bundle.channel.get_by_id(channel_id)
        self.__check_owner(channel)
        return self.bundle.message.post(channel, message)
