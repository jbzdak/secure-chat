import os
import typing

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import (
    Encoding,
    PrivateFormat,
    NoEncryption,
    PublicFormat,
    load_der_private_key,
)

__all__ = [
    "new_private_key",
    "SerializedKeys",
    "serialize_private_key",
    "deserialize_public_key",
    "deserialie_private_key",
]

_DEFAULT_CURVE: typing.Any = ec.SECP384R1


def new_uid() -> bytes:
    return os.urandom(32)


def new_private_key() -> ec.EllipticCurvePrivateKeyWithSerialization:
    return ec.generate_private_key(_DEFAULT_CURVE, default_backend())  # type: ignore


class SerializedKeys(typing.NamedTuple):

    private: bytes
    public: bytes


def serialize_private_key(
    key: ec.EllipticCurvePrivateKeyWithSerialization,
) -> SerializedKeys:
    private_bytes = key.private_bytes(Encoding.DER, PrivateFormat.PKCS8, NoEncryption())
    public_bytes = key.public_key().public_bytes(
        Encoding.X962, PublicFormat.CompressedPoint
    )
    return SerializedKeys(private_bytes, public_bytes)


def deserialie_private_key(data: bytes) -> ec.EllipticCurvePrivateKeyWithSerialization:
    return load_der_private_key(data, None, default_backend())


def deserialize_public_key(data: bytes) -> ec.EllipticCurvePublicKey:
    return ec.EllipticCurvePublicKeyWithSerialization.from_encoded_point(
        _DEFAULT_CURVE(), data
    )


def sign_ecdsa(challenge: bytes, private_key: bytes) -> bytes:
    deserialized_key = deserialie_private_key(private_key)
    return deserialized_key.sign(challenge, ec.ECDSA(hashes.SHA256()))


def verify_ecdsa(data: bytes, signature: bytes, public_key: bytes) -> bool:
    deserialized_public_key = deserialize_public_key(public_key)
    try:
        deserialized_public_key.verify(signature, data, ec.ECDSA(hashes.SHA256()))
    except InvalidSignature:
        return False
    return True
