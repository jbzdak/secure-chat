from dataclasses import replace

from secure_channel.python_api_tests import BaseUnitTestCase
from secure_channel.python_api_tests.api import register_test
from secure_channel.python_api_tests.utils import ParticipantFactory, ChannelMetaFactory
from secure_channel.server.api import ChannelService, MissingEntryException


@register_test
class TestCreateChannelService(BaseUnitTestCase):

    service: ChannelService

    def setUp(self):
        super().setUp()
        self.service = self._bundle.channel
        self.owner = ParticipantFactory(id=None)
        self.meta = ChannelMetaFactory()

    def test_create_channel(self):
        channel = self.service.create_channel(self.meta, self.owner)
        self.assertIsNotNone(channel.id)
        self.assertEqual(channel.meta, self.meta)
        self.assertEqual(len(channel.participants), 1)
        owner_from_channel = channel.participants[0]
        self.assertEqual(
            owner_from_channel.encrypt_public_key, self.owner.encrypt_public_key
        )
        self.assertEqual(owner_from_channel.sign_public_key, self.owner.sign_public_key)

    def test_created_channel_is_in_store(self):
        channel = self.service.create_channel(self.meta, self.owner)
        self.assertEqual(self.service.get_by_id(channel.id), channel)


@register_test
class TestChannelService(BaseUnitTestCase):

    service: ChannelService

    def setUp(self):
        super().setUp()
        self.service = self._bundle.channel
        owner = ParticipantFactory(id=None)
        self.other_participant = ParticipantFactory(id=None)
        self.meta = ChannelMetaFactory()
        self.channel = self.service.create_channel(self.meta, owner)
        self.owner = self.channel.participants[0]

    def test_remove_channel(self):
        self.service.remove_channel(self.channel.id)
        with self.assertRaises(MissingEntryException):
            self.service.get_by_id(self.channel.id)

    def test_add_participant(self):
        participant_id = self.service.add_participant(
            self.channel.id, self.other_participant
        )
        other_participant_with_new_id = replace(
            self.other_participant, id=participant_id
        )
        channel = self.service.get_by_id(self.channel.id)
        self.assertEqual(
            set(channel.participants), {self.owner, other_participant_with_new_id}
        )

    def test_remove_participant(self):
        self.service.remove_participant(self.channel.id, self.owner)
        channel = self.service.get_by_id(self.channel.id)
        self.assertEqual(channel.participants, tuple())

    def test_remove_notexistent_participant(self):
        self.service.remove_participant(self.channel.id, ParticipantFactory())
        channel = self.service.get_by_id(self.channel.id)
        self.assertEqual(channel.participants, (self.owner,))
