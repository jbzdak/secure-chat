import dataclasses
from datetime import datetime, timedelta

from secure_channel.python_api_tests.utils.factories import ChallengeFactory
from secure_channel.server.api import (
    MissingChallengeException,
    DuplicateChallengeException,
)

from secure_channel.python_api_tests import BaseUnitTestCase
from secure_channel.python_api_tests.api import register_test
from secure_channel.python_api_tests.utils import ChannelFactory, ParticipantFactory

from freezegun import freeze_time


@register_test
class TestChallengeService(BaseUnitTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.channel = ChannelFactory()
        self.participant = ParticipantFactory()
        self.challenge_instance = ChallengeFactory()
        self.challenge = self._bundle.challenge

    def test_not_existing_challenge(self):
        with self.assertRaises(MissingChallengeException):
            self.challenge.get_by_id(b"foobar")

    def test_create_challenge(self):
        response = self.challenge.store_challenge(
            self.participant, self.challenge_instance, self.channel
        )
        self.assertEqual(
            dataclasses.asdict(response),
            dataclasses.asdict(self.challenge.get_by_id(response.challenge.id)),
        )

    def test_duplicate_challenge(self):
        self.challenge.store_challenge(
            self.participant, self.challenge_instance, self.channel
        )
        with self.assertRaises(DuplicateChallengeException):
            self.challenge.store_challenge(
                self.participant, self.challenge_instance, self.channel
            )

    def test_challenge_expires(self):
        now = datetime(1985, 9, 19)
        with freeze_time(now):
            response = self.challenge.store_challenge(
                self.participant, self.challenge_instance, self.channel
            )
        with freeze_time(now + timedelta(seconds=self.challenge.ttl_sec + 1)):
            with self.assertRaises(MissingChallengeException):
                self.challenge.get_by_id(response.challenge.id)

    def test_challenge_expiry_method(self):
        now = datetime(1985, 9, 19)
        with freeze_time(now):
            self.challenge.store_challenge(
                self.participant, ChallengeFactory(), self.channel
            )
        with freeze_time(now + timedelta(seconds=self.challenge.ttl_sec + 1)):
            response = self.challenge.store_challenge(
                self.participant, self.challenge_instance, self.channel
            )
            self.challenge.expire()
            self.assertEqual(
                dataclasses.asdict(response),
                dataclasses.asdict(self.challenge.get_by_id(response.challenge.id)),
            )

    def test_expire_empty_service(self):
        self.challenge.expire()
