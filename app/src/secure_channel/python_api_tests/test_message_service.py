from datetime import datetime

from freezegun import freeze_time

from secure_channel.python_api_tests import BaseUnitTestCase
from secure_channel.python_api_tests.api import register_test
from secure_channel.python_api_tests.utils import ChannelFactory
from secure_channel.python_api_tests.utils.factories import MessageFactory
from secure_channel.server.api import ChannelMessageMeta
from secure_channel.server.api.services import SearchDirection
from secure_channel.server.in_memory import MessageService


class BaseTestMessagingService(BaseUnitTestCase):

    message_service: MessageService

    def setUp(self):
        super().setUp()

        self.message_service = self._bundle.message
        self.channel = ChannelFactory()


@freeze_time(datetime(1985, 9, 19, 18, 10))
@register_test
class TestMessageService(BaseTestMessagingService):
    def test_empty_channel(self):
        meta = self.message_service.get_channel_meta(self.channel)
        self.assertEqual(meta, ChannelMessageMeta(self.channel.id, None, None))

    def test_post_message_channel_meta(self):
        message = MessageFactory.create(_channel=self.channel)
        message = self.message_service.post(channel=self.channel, message=message)
        meta = self.message_service.get_channel_meta(self.channel)
        self.assertEqual(
            meta,
            ChannelMessageMeta(self.channel.id, message.iterator, message.iterator),
        )

    def test_post_message_multi_channel_meta(self):
        message1 = MessageFactory.create(_channel=self.channel)
        message2 = MessageFactory.create(_channel=self.channel)
        message1 = self.message_service.post(channel=self.channel, message=message1)
        message2 = self.message_service.post(channel=self.channel, message=message2)
        meta = self.message_service.get_channel_meta(self.channel)
        self.assertEqual(
            meta,
            ChannelMessageMeta(self.channel.id, message1.iterator, message2.iterator),
        )

    def test_post_message_different_dates(self):
        message1 = MessageFactory.create(
            _channel=self.channel, date_added=datetime(1985, 9, 19, 18, 10)
        )
        message2 = MessageFactory.create(
            _channel=self.channel, date_added=datetime(1985, 9, 18, 18, 9)
        )
        message3 = MessageFactory.create(
            _channel=self.channel, date_added=datetime(1985, 9, 18, 18, 8)
        )
        message1 = self.message_service.post(channel=self.channel, message=message1)
        message2 = self.message_service.post(channel=self.channel, message=message2)
        message3 = self.message_service.post(channel=self.channel, message=message3)
        meta = self.message_service.get_channel_meta(self.channel)
        self.assertEqual(
            meta,
            ChannelMessageMeta(self.channel.id, message1.iterator, message3.iterator),
        )
        messages = self.message_service.get_messages(
            channel=self.channel, anchor=message1.iterator
        )
        self.assertEqual(tuple(sorted(messages, key=lambda x: x.date_added)), messages)


@freeze_time(datetime(1985, 9, 19, 18, 5))
@register_test
class TestMessageServiceMessageSearch(BaseTestMessagingService):
    def setUp(self):
        super().setUp()
        self.messages = [
            self.message_service.post(
                channel=self.channel,
                message=MessageFactory.create(
                    _channel=self.channel, date_added=datetime(1985, 9, 19, 18, 4, ii)
                ),
            )
            for ii in range(10)
        ]

        self.other_channel = ChannelFactory()
        self.meta = self.message_service.get_channel_meta(self.channel)

    def test_get_message(self):
        messages = tuple(
            self.message_service.get_messages(
                channel=self.channel,
                anchor=self.meta.first_message,
                direction=SearchDirection.AFTER,
                page=10,
            )
        )
        self.assertEqual(len(messages), 10)
        self.assertEqual(
            tuple(sorted(messages, key=lambda x: x.date_added)), messages, messages
        )

    def test_get_messages_reverse(self):
        messages = tuple(
            self.message_service.get_messages(
                channel=self.channel,
                anchor=self.meta.last_message,
                direction=SearchDirection.BEFORE,
                page=10,
            )
        )
        self.assertEqual(len(messages), 10)
        self.assertEqual(
            tuple(sorted(messages, key=lambda x: x.date_added, reverse=True)),
            messages,
            messages,
        )

    def test_get_messages_small_page(self):
        messages = tuple(
            self.message_service.get_messages(
                channel=self.channel,
                anchor=self.meta.first_message,
                direction=SearchDirection.AFTER,
                page=2,
            )
        )
        self.assertEqual(len(messages), 2)
        self.assertEqual(messages, tuple(self.messages[:2]))


@register_test
class TestSubscription(BaseTestMessagingService):
    def setUp(self):
        super().setUp()
        self.callback_messages = []
        callback = lambda msg: self.callback_messages.append(msg)
        self.callback_id = self.message_service.subscribe(self.channel, callback)

    def test_subscribed(self):
        msg1 = self.message_service.post(
            self.channel, message=MessageFactory.create(_channel=self.channel)
        )
        msg2 = self.message_service.post(
            self.channel, message=MessageFactory.create(_channel=self.channel)
        )
        self.assertEqual(self.callback_messages, [msg1, msg2])

    def test_unscribed(self):
        msg1 = self.message_service.post(
            self.channel, message=MessageFactory.create(_channel=self.channel)
        )
        msg2 = self.message_service.post(
            self.channel, message=MessageFactory.create(_channel=self.channel)
        )
        self.message_service.unscribe(self.channel, self.callback_id)
        msg3 = self.message_service.post(
            self.channel, message=MessageFactory.create(_channel=self.channel)
        )
        self.assertEqual(self.callback_messages, [msg1, msg2])
