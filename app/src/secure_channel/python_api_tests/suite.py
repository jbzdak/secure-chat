import unittest
import unittest.loader
from unittest import TestSuite

from .api import ServiceBundleFactory, TEST_REGISTRY


def make_suite(factory: ServiceBundleFactory, base_class_name: str) -> TestSuite:

    suite = unittest.TestSuite()
    loader = unittest.loader.TestLoader()

    for test in TEST_REGISTRY:
        new_name = base_class_name + test.__name__
        new_test = type(new_name, (test,), {"factory": factory})
        suite.addTests(loader.loadTestsFromTestCase(new_test))

    return suite
