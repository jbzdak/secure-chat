import dataclasses
import os
from datetime import datetime
from typing import List, Tuple, Optional

from secure_channel import crypto
from secure_channel.crypto import new_uid
from secure_channel.server.api import *

__all__ = ["ChannelFactory", "ChannelMetaFactory", "ParticipantFactory"]


@dataclasses.dataclass()
class ChannelMetaFactory:
    public_algorithm: PublicKeyAlgo = PublicKeyAlgo.ECDSA
    symmetric_algorithm: SymmetricAlgo = SymmetricAlgo.AES_256


@dataclasses.dataclass()
class ChannelFactory:
    id: UID = dataclasses.field(default_factory=new_uid)
    participants: List[Participant] = dataclasses.field(default_factory=list)
    meta: ChannelMetaFactory = dataclasses.field(default_factory=ChannelMetaFactory)


@dataclasses.dataclass(unsafe_hash=True, eq=True)
class ParticipantFactory:
    id: UID = dataclasses.field(default_factory=new_uid, compare=True)
    sign_public_key: bytes = dataclasses.field(
        compare=False, default=b"not a key, check ParticipantFactory"
    )
    encrypt_public_key: bytes = dataclasses.field(
        compare=False, default=b"not a key, check ParticipantFactory"
    )
    expiry: Optional[datetime] = dataclasses.field(compare=False, default=None)

    _algorithm: PublicKeyAlgo = dataclasses.field(
        compare=False, default=PublicKeyAlgo.ECDSA
    )
    _sign_private_key: bytes = dataclasses.field(
        compare=False, default=b"not a key, check ParticipantFactory"
    )
    _encrypt_private_key: bytes = dataclasses.field(
        compare=False, default=b"not a key, check ParticipantFactory"
    )

    def __post_init__(self):
        assert self._algorithm == PublicKeyAlgo.ECDSA
        if self._sign_private_key is None:
            self._sign_private_key, self.sign_public_key = crypto.serialize_private_key(
                crypto.new_private_key()
            )
        if self._encrypt_private_key is None:
            (
                self._encrypt_private_key,
                self.encrypt_public_key,
            ) = crypto.serialize_private_key(crypto.new_private_key())


@dataclasses.dataclass(unsafe_hash=True, eq=True)
class MessageKeyFactory:

    participant_id: UID = dataclasses.field(default_factory=new_uid)
    key: bytes = dataclasses.field(default_factory=lambda: os.urandom(128))


@dataclasses.dataclass(unsafe_hash=True, eq=True)
class MessageFactory:
    id: UID = dataclasses.field(compare=True, default_factory=new_uid)
    date_added: datetime = dataclasses.field(
        compare=False, default_factory=datetime.now
    )
    message: bytes = dataclasses.field(
        compare=False, default_factory=lambda: os.urandom(32)
    )
    message_keys: Tuple[MessageKeyFactory, ...] = dataclasses.field(
        compare=False, default=tuple()
    )
    """Keys used to encrypt the message for recipients."""
    iterator: bytes = dataclasses.field(
        default_factory=lambda: os.urandom(32), compare=False
    )

    @classmethod
    def create(cls, *, _channel: Channel = None, **kwargs):
        if "message_keys" not in kwargs:
            if _channel is not None:
                kwargs["message_keys"] = [
                    MessageKeyFactory(participant_id=p.id)
                    for p in _channel.participants
                ]
        if "message_keys" in kwargs:
            kwargs["message_keys"] = tuple(kwargs["message_keys"])
        return cls(**kwargs)


@dataclasses.dataclass(unsafe_hash=True, eq=True)
class ChallengeFactory:

    id: UID = dataclasses.field(compare=True, default_factory=new_uid)
    challenge: bytes = dataclasses.field(compare=False, default_factory=new_uid)
