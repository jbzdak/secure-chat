import dataclasses
from datetime import datetime, timedelta

from secure_channel.python_api_tests import BaseUnitTestCase
from secure_channel.python_api_tests.api import register_test
from secure_channel.python_api_tests.utils import ChannelFactory, ParticipantFactory

from freezegun import freeze_time


@register_test
class TestSessionService(BaseUnitTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.channel = ChannelFactory()
        self.participant = ParticipantFactory()
        self.session_service = self._bundle.session

    def test_session_create_session(self):
        session = self.session_service.create_session()
        self.assertIsNone(session.channel_with_access)
        self.assertIsNone(session.logged_in_particpant)
        session_id = self.session_service.update_session(session)
        self.assertIsNotNone(session_id, None)

    def test_session_non_existent_session(self):
        session = self.session_service.get_by_id(session_cookie="foobar")
        self.assertIsNone(session.channel_with_access)
        self.assertIsNone(session.logged_in_particpant)
        session_id = self.session_service.update_session(session)
        self.assertIsNotNone(session_id)
        self.assertNotEqual(session_id, "foobar")

    def test_session_is_stored(self):
        session = self.session_service.create_session()
        session.logged_in_particpant = self.participant.id
        session.channel_with_access = self.channel.id
        new_cookie = self.session_service.update_session(session)
        session_from_store = self.session_service.get_by_id(new_cookie)
        self.assertEqual(
            dataclasses.asdict(session), dataclasses.asdict(session_from_store)
        )

    def test_expired_session(self):
        now = datetime(1985, 9, 19)
        with freeze_time(now):
            session = self.session_service.create_session()
            session.logged_in_particpant = self.participant.id
            session.channel_with_access = self.channel.id
            new_cookie = self.session_service.update_session(session)
            session_from_store = self.session_service.get_by_id(new_cookie)
            self.assertEqual(
                dataclasses.asdict(session), dataclasses.asdict(session_from_store)
            )
        with freeze_time(now + timedelta(seconds=self.session_service.ttl_sec + 1)):
            session_from_store = self.session_service.get_by_id(new_cookie)
            self.assertIsNone(session_from_store.logged_in_particpant)
            self.assertIsNone(session_from_store.channel_with_access)

    def test_expiry(self):
        now = datetime(1985, 9, 19)
        with freeze_time(now):
            self.session_service.create_session()
            self.session_service.create_session()
            self.session_service.create_session()
            self.session_service.create_session()
        with freeze_time(now + timedelta(seconds=self.session_service.ttl_sec + 1)):
            session = self.session_service.create_session()
            session.logged_in_particpant = self.participant.id
            session.channel_with_access = self.channel.id
            new_cookie = self.session_service.update_session(session)
            self.session_service.expire_old_sessions()
            session_from_store = self.session_service.get_by_id(new_cookie)
            self.assertEqual(
                dataclasses.asdict(session), dataclasses.asdict(session_from_store)
            )

    def test_expire_empty_service(self):
        self.session_service.expire_old_sessions()
