import dataclasses
import typing
import unittest.loader

from secure_channel.server import api
import dataclasses
import typing
import unittest.loader

from secure_channel.server import api

TEST_REGISTRY = set()


def register_test(type):
    TEST_REGISTRY.add(type)
    return type


@dataclasses.dataclass(frozen=True)
class ServiceBundle:

    channel: api.ChannelService
    message: api.MessageService
    session: api.SessionService
    challenge: api.ChallengeService

    def tear_down(self):
        pass


ServiceBundleFactory = typing.Callable[[typing.Any], ServiceBundle]


class BaseUnitTestCase(unittest.TestCase):
    factory: ServiceBundleFactory
    _bundle: ServiceBundle

    def setUp(self):
        super().setUp()
        self._bundle = self.factory()

    def tearDown(self):
        super().tearDown()
        self._bundle.tear_down()
