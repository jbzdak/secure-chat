import pathlib
from invoke import task, Context

BASE_DIR = pathlib.Path(__file__).parent.absolute()

SRC_DIR = BASE_DIR / "src"
TEST_DIR = BASE_DIR / "test"
OUTPUTS_DIR = BASE_DIR.parent / "outputs"


@task
def test(ctx, no_coverage=False):
    assert isinstance(ctx, Context)
    with ctx.cd(str(TEST_DIR)):
        command = ["python", "-m", "unittest", "discover"]
        coverage_command = [
            "coverage",
            "run",
            "--source={}".format(str(SRC_DIR)),
            "--omit='{}/secure_channel/python_api_tests/*'".format(str(SRC_DIR)),
        ]
        if not no_coverage:
            ctx.run("coverage erase")
            command = command[1:]
            command = coverage_command + command
        ctx.run(" ".join(command), env={"PYTHONPATH": str(SRC_DIR)})
        if not no_coverage:
            ctx.run("coverage html -d {}".format(OUTPUTS_DIR / "htmlcov"))
            ctx.run("coverage report")


@task
def black(ctx):
    with ctx.cd(str(BASE_DIR)):
        ctx.run("black .")


@task
def black_check(ctx):
    with ctx.cd(str(BASE_DIR)):
        ctx.run("black --check .")


@task
def mypy_check(ctx):
    with ctx.cd(str(BASE_DIR)):
        ctx.run("mypy src")


@task(black_check, mypy_check, test)
def check(ctx):
    pass


@task
def sync_deps(ctx):
    ctx.run("poetry export -f requirements.txt -o requirements.txt --without-hashes")
    ctx.run(
        "poetry export -f requirements.txt -o requirements-dev.txt --dev --without-hashes"
    )
