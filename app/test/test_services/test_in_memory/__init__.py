from secure_channel.python_api_tests.suite import make_suite
from . import bundle


def load_tests(loader, tests, pattern):
    return make_suite(bundle.bundle_factory, "InMemory")
