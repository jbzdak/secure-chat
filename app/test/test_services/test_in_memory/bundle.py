from secure_channel.python_api_tests import ServiceBundle
from secure_channel.server.in_memory import (
    ChannelServiceImpl,
    MessageServiceImpl,
    SessionServiceImpl,
    ChallengeServiceImpl,
)


def bundle_factory(*args, **kwargs):
    return ServiceBundle(
        channel=ChannelServiceImpl(),
        message=MessageServiceImpl(),
        session=SessionServiceImpl.create(900),
        challenge=ChallengeServiceImpl.create(900),
    )
