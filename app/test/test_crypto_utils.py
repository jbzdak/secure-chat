import os
import unittest

from cryptography.hazmat.primitives.asymmetric.ec import (
    EllipticCurvePrivateKeyWithSerialization,
)

from secure_channel import crypto


class TestCryptoUtils(unittest.TestCase):
    def test_private_key(self):
        self.assertIsInstance(
            crypto.new_private_key(), EllipticCurvePrivateKeyWithSerialization
        )

    def test_serialize_private_key(self):

        private_key = crypto.new_private_key()
        private, __ = crypto.serialize_private_key(private_key)
        deserialized_pk = crypto.deserialie_private_key(private)
        self.assertEqual(
            private_key.private_numbers(), deserialized_pk.private_numbers()
        )

    def test_deserialize_public_key(self):
        private_key = crypto.new_private_key()
        __, public_bytes = crypto.serialize_private_key(private_key)
        deserialized_key = crypto.deserialize_public_key(public_bytes)
        self.assertEqual(
            deserialized_key.public_numbers(), private_key.public_key().public_numbers()
        )

    def test_challenge_flow_positive(self):
        private_key = crypto.new_private_key()
        private, public = crypto.serialize_private_key(private_key)

        challenge = os.urandom(32)

        signature = crypto.sign_ecdsa(challenge, private)
        self.assertTrue(crypto.verify_ecdsa(challenge, signature, public))

    def test_challenge_flow_negative(self):
        private_key = crypto.new_private_key()
        private, __ = crypto.serialize_private_key(private_key)
        other = crypto.new_private_key()
        __, public = crypto.serialize_private_key(other)

        challenge = os.urandom(32)

        signature = crypto.sign_ecdsa(challenge, private)
        self.assertFalse(crypto.verify_ecdsa(challenge, signature, public))
